image: debian:12

stages:
  - build
  - package
  - test
  - pages

variables:
  # https://github.com/opencontainers/runc/releases
  RUNC_VERSION: 1.1.15
  # https://github.com/containerd/containerd/releases
  CONTAINERD_VERSION: 1.7.23
  CONTAINERD_PACKAGE_VERSION: ${CONTAINERD_VERSION}-1
  # https://github.com/kubernetes-sigs/cri-tools/releases/
  CRI_TOOLS_VERSION: 1.30.0

include:
  - project: nerzhuls-packages/ci_templates
    file: /debian_repo.yml
    ref: master
  - project: nerzhuls-packages/ci_templates
    file: /docker_buildx_templates.yml
    ref: master

.package_template:
  stage: package
  before_script:
    - ./ci/before.sh
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - dist/*

build:runc:amd64:
  image: golang:1.23.2-bullseye
  stage: build
  before_script:
    - git clone https://github.com/opencontainers/runc -b v${RUNC_VERSION}
    - apt-get update -qyy
    - apt-get install -qyy btrfs-progs libseccomp-dev libapparmor-dev libbtrfs-dev
    - mkdir -p binary-artifacts/amd64
  script:
    - cd runc
    - make BUILDTAGS='seccomp apparmor'
    - make install BUILDTAGS='seccomp apparmor'
    - cd -
    - cp /usr/local/sbin/runc binary-artifacts/amd64/
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - binary-artifacts/amd64/*

build:containerd:amd64:
  image: golang:1.23.2-bullseye
  stage: build
  variables:
    BUILDTAGS: "no_btrfs no_aufs no_zfs no_devmapper"
  before_script:
    - git clone https://github.com/containerd/containerd -b v${CONTAINERD_VERSION} containerd_src
    - apt-get update -qyy
    - apt-get install -qyy btrfs-progs libseccomp-dev libapparmor-dev libbtrfs-dev wget
    - wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v${CRI_TOOLS_VERSION}/crictl-v${CRI_TOOLS_VERSION}-linux-amd64.tar.gz 2>&1 > /dev/null
    - tar xvzf crictl-v${CRI_TOOLS_VERSION}-linux-amd64.tar.gz
    - mkdir -p binary-artifacts/amd64
    - cp crictl binary-artifacts/amd64
  script:
    - cd containerd_src
    - make
    - make install
    - cp bin/{ctr,containerd,containerd-shim,containerd-shim-runc-v1,containerd-shim-runc-v2}  ../binary-artifacts/amd64
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - binary-artifacts/amd64/*

build:containerd:arm64:
  stage: build
  extends: .buildx_build_extract_files
  variables:
    BUILDX_PLATFORM: linux/arm64
  artifacts:
    when: on_success
    expire_in: 1 day
    paths:
      - artifacts/*

package:amd64:
  extends: .package_template
  dependencies:
    - build:containerd:amd64
    - build:runc:amd64
  needs:
    - build:containerd:amd64
    - build:runc:amd64
  script:
    - mkdir -p containerd/usr/bin
    - cp binary-artifacts/amd64/runc containerd/usr/bin
    - cp binary-artifacts/amd64/crictl containerd/usr/bin
    - cp binary-artifacts/amd64/ctr containerd/usr/bin
    - cp binary-artifacts/amd64/containerd containerd/usr/bin
    - cp binary-artifacts/amd64/containerd-shim containerd/usr/bin
    - cp binary-artifacts/amd64/containerd-shim-runc-v1 containerd/usr/bin
    - cp binary-artifacts/amd64/containerd-shim-runc-v2 containerd/usr/bin
    - ./ci/package.sh amd64

package:arm64:
  extends: .package_template
  dependencies:
    - build:containerd:arm64
  needs:
    - build:containerd:arm64
  script:
    - find artifacts
    - mkdir -p containerd/usr/bin
    - cp artifacts/runc containerd/usr/bin
    - cp artifacts/ctr containerd/usr/bin
    - cp artifacts/containerd containerd/usr/bin
    - cp artifacts/containerd-shim containerd/usr/bin
    - cp artifacts/containerd-shim-runc-v1 containerd/usr/bin
    - cp artifacts/containerd-shim-runc-v2 containerd/usr/bin
    - ./ci/package.sh arm64

test:
  stage: test
  dependencies:
    - package:amd64
  needs:
    - package:amd64
  before_script:
    - apt-get update -qyy
    - apt-get install -qy libseccomp2 btrfs-progs apparmor
  script:
    - dpkg -i dist/*.deb

pages:
  stage: pages
  extends: .build_debian_repo
  needs:
    - package:amd64
    - package:arm64
  dependencies:
    - package:amd64
    - package:arm64


