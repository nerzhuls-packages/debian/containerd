FROM arm64v8/golang:1.22.3-bullseye

ARG RUNC_VERSION=1.1.9
ARG CONTAINERD_VERSION=1.7.7
ARG CRI_TOOLS_VERSION=1.28.0

# This file is only to be run with docker buildx on a specialized env
RUN apt-get update -qyy && \
    apt-get install -qyy git btrfs-progs libseccomp-dev libapparmor-dev libbtrfs-dev wget && \
    wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v${CRI_TOOLS_VERSION}/crictl-v${CRI_TOOLS_VERSION}-linux-arm.tar.gz && \
    tar xvzf crictl-v${CRI_TOOLS_VERSION}-linux-arm.tar.gz && \
    mkdir -p /artifacts/ && \
    cp crictl /artifacts/

RUN git clone https://github.com/opencontainers/runc -b v${RUNC_VERSION} && \
    cd runc && \
    make BUILDTAGS='seccomp apparmor' && \
    make install BUILDTAGS='seccomp apparmor' && \
    cd - && \
    cp /usr/local/sbin/runc /artifacts/

RUN git clone https://github.com/containerd/containerd.git -b v${CONTAINERD_VERSION} && \
    cd containerd && \
    make && \
    make install && \
    cp /usr/local/bin/ctr /artifacts/ && \
    cp /usr/local/bin/containerd /artifacts/ && \
    cp /usr/local/bin/containerd-shim /artifacts/ && \
    cp /usr/local/bin/containerd-shim-runc-v1 /artifacts/ && \
    cp /usr/local/bin/containerd-shim-runc-v2 /artifacts/ && \
    find /artifacts/ -type f

