#! /bin/bash

set -e
set -u

PKG_ARCH=${1}

for PKG in containerd; do
    echo "Packaging ${PKG}"
    # TODO
    # Replace versions
    sed -i 's/%%CONTAINERD_VERSION%%/'${CONTAINERD_PACKAGE_VERSION}'/g' ${PKG}/DEBIAN/control
    sed -i 's/amd64/'${PKG_ARCH}'/g' ${PKG}/DEBIAN/control
    dpkg-deb -b ${PKG}
    mv ${PKG}.deb dist/${PKG}-${PKG_ARCH}.deb
done
